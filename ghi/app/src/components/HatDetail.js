import React from "react";
import {useParams, useNavigate} from "react-router-dom";



function HatDetail(props){
    const params = useParams()
    const navigate = useNavigate()
    return (
        <HatDetailDisplay hatId={params.hatId} navigate={navigate} />
      );
    }


class HatDetailDisplay extends React.Component {
    state = {
        hat: {
            location: {}
        }
    }

    handleDelete = async (event) => {
        console.log(this.props)
        const url = `http://localhost:8090/api/hats/${this.props.hatId}/`
        const response = await fetch(url,{
            method: 'delete',
        })
        if (response.ok) {
            const data = await response.json()

            // this.forceUpdate()
            // this.setState({hat: {}})
        }
        return window.location.href="/hats"
    }

    async componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            const response = await fetch(`http://localhost:8090/api/hats/${this.props.hatId}/`)
            if (response.ok) {
                const data = await response.json()

                this.setState({
                    hat: data
                })
            }
        }

    }


    render() {
        // console.log(this.state)
        return (
            <div>
                <img src={this.state.hat.picture_url} className="card-img-top" alt="Sorry This Is Broken" />
                <h1 className="card-title">{this.state.hat.color} {this.state.hat.style_name}</h1>
                <div className="card-body">
                    <p className="card-text">
                        - This hat is made of {this.state.hat.fabric} material
                    </p>
                    <h3 className="card-subtitle mb-2 text-muted">
                        Location: {this.state.hat.location.closet_name}</h3>
                        <h5 className="card-subtitle mb-2 text-muted">Section: {this.state.hat.location.section_number}</h5>
                        <h5 className="card-subtitle mb-2 text-muted">Shelf: {this.state.hat.location.shelf_number}</h5>

                </div>
                <div>
                    <button variant="danger" onClick={() => this.handleDelete(this.state.hat.id)} className="btn btn-primary">Delete This Hat</button>
                </div>
            </div>
        )
    }
}

export default HatDetail;
